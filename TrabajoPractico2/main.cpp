#include <iostream>
#include <windows.h>
#include "juego.h"

#if _DEBUG
#define USE_CONSOLE true
#else
#define USE_CONSOLE false
#endif

using namespace std;


void main() {
	Juego * wind = new Juego();
	if (!USE_CONSOLE)
	{
		ShowWindow(GetConsoleWindow(), SW_HIDE);
	}
	wind->Run(800, 600);
}