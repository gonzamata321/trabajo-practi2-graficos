#ifndef JUGADOR_H
#define JUGADOR_H

#include <iostream>
#include "string.h"
#include <SFML/Graphics.hpp>

using namespace std;

class Jugador
{
private:
	sf::Texture texture;
	sf::Sprite sprite;
	float x;
	float y;
public:
	Jugador(string img);
	void Movimiento();
	void Update();
	sf::Sprite GetSprite();
	~Jugador();
};
#endif

