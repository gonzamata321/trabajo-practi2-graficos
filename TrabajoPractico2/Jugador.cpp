#include "Jugador.h"



Jugador::Jugador(string img)
{
	x = 0;
	y = 0;
	if (!texture.loadFromFile(img)) {
		cout << "ERROR" << endl;
	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
	sprite.setPosition(x,y);
}

void Jugador::Movimiento()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		x = x - 0.1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		x = x + 0.1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		y = y - 0.1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		y = y + 0.1;
	}
}

void Jugador::Update()
{
	sprite.setPosition(x, y);
}

sf::Sprite Jugador::GetSprite()
{
	return sprite;
}


Jugador::~Jugador()
{
}
