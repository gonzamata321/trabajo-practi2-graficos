#include "Juego.h"



Juego::Juego()
{
	Jugador1 = new Jugador("../Asets/img/personaje.png");
}

void Juego::Run(int high, int width)
{
	sf::RenderWindow window(sf::VideoMode(high, width), "SFML works!");
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			window.close();
		}
		Jugador1->Movimiento();
		Jugador1->Update();
		window.clear();
		window.draw(Jugador1->GetSprite());
		window.display();
	}
}


Juego::~Juego()
{
}
