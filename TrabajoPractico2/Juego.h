#ifndef JUEGO_H
#define JUEGO_H

#include <iostream>
#include "jugador.h"
#include <SFML/Graphics.hpp>

class Juego
{
private:
	Jugador* Jugador1;
public:
	Juego();
	void Run(int high, int width);
	~Juego();
};
#endif
